<?php




function get_project_user($idproject){
    global $cnx;
    $sth = $cnx->prepare("
        SELECT * 
        FROM participations
        JOIN users
            ON participations.FK_user = users.ID
        WHERE FK_project = :idproject
    ");
    $sth->execute(["idproject"=>$idproject]);
    return $sth->fetchAll();
}



function get_user($id){
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM users WHERE ID = :id;");
    $sth->execute(["id"=>$id]);
    return $sth->fetchAll();
}



function get_all_users(){
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM users;");
    $sth->execute();
    return $sth->fetchAll();
}



function add_user($firstname, $lastname, $role, $level, $address, $postcode, $city){
    global $cnx;
    $sth = $cnx->prepare("
        INSERT INTO users (firstname, lastname, role, level, address, postcode, city) 
        VALUES (:firstname, :lastname, :role, :level, :address, :postcode, :city)
    ");

    $result= $sth->execute([
        "firstname"=>$firstname, 
        "lastname"=>$lastname, 
        "role"=>$role, 
        "level"=>$level, 
        "address"=>$address, 
        "postcode"=>$postcode, 
        "city"=>$city
    ]);

    var_dump($sth);

    $arr = $sth->errorInfo();
print_r($arr);
    return $cnx->lastInsertId();
}



function delete_user($id){
    global $cnx;
    $sth = $cnx->prepare("DELETE FROM users WHERE ID = :iduser;");
    return $sth->execute(["iduser"=>$id]);
}



function update_user($id, $firstname, $lastname, $role, $level, $address, $postcode, $city){
    global $cnx;
    $sth = $cnx->prepare("UPDATE users SET firstname = :firstname, lastname = :lastname, role= :role, level= :level, address= :address, postcode= :postcode, city= :city WHERE ID = :id;");
    return $sth->execute(["id"=>$id, "firstname"=>$firstname, "lastname"=>$lastname, "role"=>$role, "level"=>$level, "address"=>$address, "postcode"=>$postcode, "city"=>$city]);
}



function print_user( $user ){
    
    switch($user['role']){
        case "développeur": 
            echo ' <span class="glyphicon glyphicon-wrench"></span>';
            break;

        case "client": 
            echo ' <span class="glyphicon glyphicon-euro"></span>';
            break;

        default:
            echo ' <span class="glyphicon glyphicon-question-sign"></span>';
    }

    echo " " . $user['firstname'] . " " . $user['lastname'] . " ";
}

