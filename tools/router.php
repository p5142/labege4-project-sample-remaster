<?php

$message = "";

switch($_GET['action']){
    case "addproject"; 
    $message = add_project($_REQUEST['name'], $_REQUEST['detail'])
        ? "Projet ajouté avec succès" 
        : "L'ajout du projet a échoué :(";
    break;

    case "changeproject"; 
    $message = update_project($_REQUEST['ID'], $_REQUEST['name'], $_REQUEST['detail'])
        ? "Projet mis à jour avec succès !" 
        : "La modification du projet a échoué :(";
    break;

    case "deleteproject"; 
    $message = delete_project($_REQUEST['ID'])
        ? "Suppression réalisée avec succès !" 
        : "La suppression du projet a échoué :(";
    break;

    case "deleteuser"; 
    $message = delete_user($_REQUEST['user_id'])
        ? "Suppression réalisée avec succès !" 
        : "La suppression de l'user a échoué :(";
    break;

    case "addparticipation"; 
    $message = add_participation($_REQUEST['idproject'], $_REQUEST['iduser'])
        ? "Participation acceptée avec succès !" 
        : "Action échouée : sélectionnez un utilisateur qui ne participe pas déjà à ce projet !";
    break;

    case "deleteparticipation"; 
    $message = delete_participation($_REQUEST['idproject'], $_REQUEST['iduser'])
        ? "Personne retirée de ce projet !" 
        : "Le retrait de cette personne a échoué :(";
    break;

    case "adduser"; 
    $message = add_user($_REQUEST['firstname'], $_REQUEST['lastname'], $_REQUEST['role'], $_REQUEST['level'], $_REQUEST['address'], $_REQUEST['postcode'], $_REQUEST['city'])
        ? "Personne ajoutée avec succès" 
        : "Ajout de la personné échoué :(";
    break;

    case "changeuser"; 
    $message = update_user($_REQUEST['user_id'], $_REQUEST['user_firstname'], $_REQUEST['user_lastname'], $_REQUEST['user_role'], $_REQUEST['user_level'], $_REQUEST['user_address'], $_REQUEST['user_postcode'], $_REQUEST['user_city'])
        ? "User mis à jour avec succès !" 
        : "La modification de l'user a échoué :(";
    break;

}